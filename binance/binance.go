package binance

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"time"

	. "gitee.com/378077287/exchanges"
	"gitee.com/378077287/exchanges/utils"

	binance "github.com/adshao/go-binance"
	"github.com/golang/glog"
)

// Binance the Binance  exchange
type Binance struct {
	client           *binance.Client
	symbol           string
	listenKey        string //币安交易所websocket订阅用户信息的key
	userStream       bool   // 是否已经订阅用户信息
	orderCallBack    func(orders []*Order)
	positionCallBack func(positions []*Position)
	eventTime        int64 // 币安私人订阅数据变化会推两次，如一个订单挂单，会推两次。根据事件时间，屏蔽同一推送
}

func (b *Binance) GetName() (name string) {
	return "binance"
}

func (b *Binance) GetTime() (tm int64, err error) {
	tm, err = b.client.NewServerTimeService().
		Do(context.Background())
	return
}

// SetProxy ...
// proxyURL: http://127.0.0.1:1080
func (b *Binance) SetProxy(proxyURL string) error {
	proxyURL_, err := url.Parse(proxyURL)
	if err != nil {
		return err
	}

	//adding the proxy settings to the Transport object
	transport := &http.Transport{
		Proxy: http.ProxyURL(proxyURL_),
	}

	//adding the Transport object to the http Client
	b.client.HTTPClient.Transport = transport
	return nil
}

// currency: USDT
func (b *Binance) GetBalance(currency string) (result *Balance, err error) {
	// var res []*binance.Balance
	// res, err = b.client.NewGetAccountService().
	// 	Do(context.Background())
	// if err != nil {
	// 	return
	// }
	// result = &Balance{}
	// for _, v := range res {
	// 	if v.Asset == currency { // USDT
	// 		value := utils.ParseFloat64(v.Balance)
	// 		result.Equity = value
	// 		result.Available = value
	// 		break
	// 	}
	// }
	return
}

func (b *Binance) GetOrderBook(symbol string, depth int) (result *OrderBook, err error) {
	result = &OrderBook{}
	if depth <= 5 {
		depth = 5
	} else if depth <= 10 {
		depth = 10
	} else if depth <= 20 {
		depth = 20
	} else if depth <= 50 {
		depth = 50
	} else if depth <= 100 {
		depth = 100
	} else if depth <= 500 {
		depth = 500
	} else {
		depth = 1000
	}
	var res *binance.DepthResponse
	res, err = b.client.NewDepthService().
		Symbol(symbol).
		Limit(depth).
		Do(context.Background())
	if err != nil {
		return
	}
	for _, v := range res.Asks {
		result.Asks = append(result.Asks, Item{
			Price:  utils.ParseFloat64(v.Price),
			Amount: utils.ParseFloat64(v.Quantity),
		})
	}
	for _, v := range res.Bids {
		result.Bids = append(result.Bids, Item{
			Price:  utils.ParseFloat64(v.Price),
			Amount: utils.ParseFloat64(v.Quantity),
		})
	}
	result.Time = time.Now()
	return
}

func (b *Binance) GetRecords(symbol string, period string, from int64, end int64, limit int) (records []*Record, err error) {
	var res []*binance.Kline
	service := b.client.NewKlinesService().
		Symbol(symbol).
		Interval(b.IntervalKlinePeriod(period)).
		Limit(limit)
	if from > 0 {
		service = service.StartTime(from * 1000)
	}
	if end > 0 {
		service = service.EndTime(end * 1000)
	}
	res, err = service.Do(context.Background())
	if err != nil {
		return
	}
	for _, v := range res {
		records = append(records, &Record{
			Symbol:    symbol,
			Timestamp: time.Unix(0, v.OpenTime*int64(time.Millisecond)),
			Open:      utils.ParseFloat64(v.Open),
			High:      utils.ParseFloat64(v.High),
			Low:       utils.ParseFloat64(v.Low),
			Close:     utils.ParseFloat64(v.Close),
			Volume:    utils.ParseFloat64(v.Volume),
		})
	}
	return
}

func (b *Binance) IntervalKlinePeriod(period string) string {
	m := map[string]string{
		PERIOD_1WEEK: "7d",
	}
	if v, ok := m[period]; ok {
		return v
	}
	return period
}

func (b *Binance) SetContractType(currencyPair string, contractType string) (err error) {
	b.symbol = currencyPair
	return
}

func (b *Binance) GetContractID() (symbol string, err error) {
	return b.symbol, nil
}

func (b *Binance) SetLeverRate(value float64) (err error) {
	return
}

func (b *Binance) OpenLong(symbol string, orderType OrderType, price float64, size float64) (result *Order, err error) {
	return b.PlaceOrder(symbol, Buy, orderType, price, size)
}

func (b *Binance) OpenShort(symbol string, orderType OrderType, price float64, size float64) (result *Order, err error) {
	return b.PlaceOrder(symbol, Sell, orderType, price, size)
}

func (b *Binance) CloseLong(symbol string, orderType OrderType, price float64, size float64) (result *Order, err error) {
	return b.PlaceOrder(symbol, Sell, orderType, price, size, OrderReduceOnlyOption(true))
}

func (b *Binance) CloseShort(symbol string, orderType OrderType, price float64, size float64) (result *Order, err error) {
	return b.PlaceOrder(symbol, Buy, orderType, price, size, OrderReduceOnlyOption(true))
}

func (b *Binance) PlaceOrder(symbol string, direction Direction, orderType OrderType, price float64,
	size float64, opts ...PlaceOrderOption) (result *Order, err error) {
	params := ParsePlaceOrderParameter(opts...)
	service := b.client.NewCreateOrderService().
		Symbol(symbol).
		Quantity(fmt.Sprint(size))
	var side binance.SideType
	if direction == Buy {
		side = binance.SideTypeBuy
	} else if direction == Sell {
		side = binance.SideTypeSell
	}
	var _orderType binance.OrderType
	switch orderType {
	case OrderTypeLimit:
		_orderType = binance.OrderTypeLimit
	case OrderTypeMarket:
		_orderType = binance.OrderTypeMarket
	case OrderTypeStopMarket:
		_orderType = binance.OrderTypeStopLoss
		service = service.StopPrice(fmt.Sprint(params.StopPx))
	case OrderTypeStopLimit:
		_orderType = binance.OrderTypeStopLossLimit
		service = service.StopPrice(fmt.Sprint(params.StopPx))
	}
	if price > 0 {
		service = service.Price(fmt.Sprint(price))
	}

	service = service.Side(side).Type(_orderType)
	var res *binance.CreateOrderResponse
	res, err = service.Do(context.Background())
	if err != nil {
		return
	}
	result = b.convertOrder1(res)
	return
}

func (b *Binance) GetOpenOrders(symbol string, opts ...OrderOption) (result []*Order, err error) {
	service := b.client.NewListOpenOrdersService().
		Symbol(symbol)
	var res []*binance.Order
	res, err = service.Do(context.Background())
	if err != nil {
		return
	}
	for _, v := range res {
		result = append(result, b.convertOrder(v))
	}
	return
}

func (b *Binance) GetOrder(symbol string, id string, opts ...OrderOption) (result *Order, err error) {
	var orderID int64
	orderID, err = strconv.ParseInt(id, 10, 64)
	if err != nil {
		return
	}
	var res *binance.Order
	res, err = b.client.NewGetOrderService().
		Symbol(symbol).
		OrderID(orderID).
		Do(context.Background())
	if err != nil {
		return
	}
	result = b.convertOrder(res)
	return
}

func (b *Binance) CancelOrder(symbol string, id string, opts ...OrderOption) (result *Order, err error) {
	var orderID int64
	orderID, err = strconv.ParseInt(id, 10, 64)
	if err != nil {
		return
	}
	var res *binance.CancelOrderResponse
	res, err = b.client.NewCancelOrderService().
		Symbol(symbol).
		OrderID(orderID).
		Do(context.Background())
	if err != nil {
		return
	}
	result = b.convertOrder2(res)
	return
}

func (b *Binance) CancelAllOrders(symbol string, opts ...OrderOption) (err error) {
	// err = b.client.NewCancelAllOpenOrdersService().
	// 	Symbol(symbol).
	// 	Do(context.Background())
	return
}

func (b *Binance) AmendOrder(symbol string, id string, price float64, size float64, opts ...OrderOption) (result *Order, err error) {
	return
}

func (b *Binance) GetPositions(symbol string) (result []*Position, err error) {
	// var res []*futures.PositionRisk
	// res, err = b.client.NewGetPositionRiskService().
	// 	Do(context.Background())
	// if err != nil {
	// 	return
	// }

	// useFilter := symbol != ""

	// for _, v := range res {
	// 	if useFilter && v.Symbol != symbol {
	// 		continue
	// 	}
	// 	position := &Position{}
	// 	position.Symbol = v.Symbol
	// 	size := utils.ParseFloat64(v.PositionAmt)
	// 	if size != 0 {
	// 		position.Size = size
	// 		position.OpenPrice = utils.ParseFloat64(v.EntryPrice)
	// 		position.AvgPrice = position.OpenPrice
	// 	}
	// 	result = append(result, position)
	// }
	return
}

func (b *Binance) convertOrder(order *binance.Order) (result *Order) {
	result = &Order{}
	result.ID = fmt.Sprint(order.OrderID)
	result.Symbol = order.Symbol
	result.Price = utils.ParseFloat64(order.Price)
	result.StopPx = utils.ParseFloat64(order.StopPrice)
	result.Amount = utils.ParseFloat64(order.OrigQuantity)
	result.Direction = b.convertDirection(order.Side)
	result.Type = b.convertOrderType(order.Type)
	result.AvgPrice = utils.ParseFloat64(order.Price)
	result.FilledAmount = utils.ParseFloat64(order.ExecutedQuantity)

	result.Status = b.orderStatus(order.Status)
	return
}

func (b *Binance) convertOrder1(order *binance.CreateOrderResponse) (result *Order) {
	result = &Order{}
	result.ID = fmt.Sprint(order.OrderID)
	result.Symbol = order.Symbol
	result.Price = utils.ParseFloat64(order.Price)
	result.Amount = utils.ParseFloat64(order.OrigQuantity)
	result.Direction = b.convertDirection(order.Side)
	result.Type = b.convertOrderType(order.Type)
	result.AvgPrice = utils.ParseFloat64(order.Price)
	result.FilledAmount = utils.ParseFloat64(order.ExecutedQuantity)

	result.Status = b.orderStatus(order.Status)
	return
}

func (b *Binance) convertOrder2(order *binance.CancelOrderResponse) (result *Order) {
	result = &Order{}
	result.ID = fmt.Sprint(order.OrderID)
	result.Symbol = order.Symbol
	result.Price = utils.ParseFloat64(order.Price)
	result.Amount = utils.ParseFloat64(order.OrigQuantity)
	result.Direction = b.convertDirection(order.Side)
	result.Type = b.convertOrderType(order.Type)
	result.AvgPrice = 0
	result.FilledAmount = utils.ParseFloat64(order.ExecutedQuantity)

	result.Status = b.orderStatus(order.Status)
	return
}

func (b *Binance) convertDirection(side binance.SideType) Direction {
	switch side {
	case binance.SideTypeBuy:
		return Buy
	case binance.SideTypeSell:
		return Sell
	default:
		return Buy
	}
}

func (b *Binance) convertOrderType(orderType binance.OrderType) OrderType {
	/*
		OrderTypeTakeProfitMarket   OrderType = "TAKE_PROFIT_MARKET"
		OrderTypeTrailingStopMarket OrderType = "TRAILING_STOP_MARKET"
	*/
	switch orderType {
	case binance.OrderTypeLimit:
		return OrderTypeLimit
	case binance.OrderTypeMarket:
		return OrderTypeMarket
	case binance.OrderTypeStopLossLimit:
		return OrderTypeStopLimit
	case binance.OrderTypeStopLoss:
		return OrderTypeStopMarket
	default:
		return OrderTypeLimit
	}
}

func (b *Binance) orderStatus(status binance.OrderStatusType) OrderStatus {
	switch status {
	case binance.OrderStatusTypeNew:
		return OrderStatusNew
	case binance.OrderStatusTypePartiallyFilled:
		return OrderStatusPartiallyFilled
	case binance.OrderStatusTypeFilled:
		return OrderStatusFilled
	case binance.OrderStatusTypeCanceled:
		return OrderStatusCancelled
	case binance.OrderStatusTypeRejected:
		return OrderStatusRejected
	case binance.OrderStatusTypeExpired:
		return OrderStatusCancelled
	default:
		return OrderStatusCreated
	}
}

// SubscribeTrades 订阅逐笔成交.
func (b *Binance) SubscribeTrades(market Market, callback func(trades []*Trade)) error {

	wsTradeHandler := func(event *WsTradeEvent) {
		var trades []*Trade
		trade := &Trade{
			ID:     utils.ParseIntString(event.TradeID),
			Price:  utils.ParseFloat64(event.Price),
			Amount: utils.ParseFloat64(event.Quantity),
			Ts:     event.TradeTime,
			Symbol: event.Symbol,
		}
		if event.IsBuyerMaker {
			trade.Direction = Sell
		} else {
			trade.Direction = Buy
		}
		trades = append(trades, trade)
		callback(trades)
	}
	errHandler := func(err error) {
		fmt.Println(err)
		glog.Error(err)
	}
	//WsAggTradeServe(market.Symbol, wsAggTradeHandler, errHandler)
	WsTradeServe(market.Symbol, wsTradeHandler, errHandler)
	return nil
}

// SubscribeLevel2Snapshots 订阅orderbook .
func (b *Binance) SubscribeLevel2Snapshots(market Market, callback func(ob *OrderBook)) error {
	wsDepthHandler := func(event *WsDepthEvent) {
		ob := &OrderBook{
			Symbol: event.Symbol,
			Time:   time.Unix(event.Time, 0),
		}
		for _, v := range event.Bids {
			item := Item{
				Price:  utils.ParseFloat64(v.Price),
				Amount: utils.ParseFloat64(v.Quantity),
			}
			ob.Bids = append(ob.Bids, item)
		}
		for _, v := range event.Asks {
			item := Item{
				Price:  utils.ParseFloat64(v.Price),
				Amount: utils.ParseFloat64(v.Quantity),
			}
			ob.Asks = append(ob.Asks, item)
		}

		callback(ob)
	}
	errHandler := func(err error) {
		fmt.Println(err)
		glog.Error(err)
	}
	_, _, err := WsDepthServe(market.Symbol, wsDepthHandler, errHandler)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func (b *Binance) SubscribeOrders(market Market, callback func(orders []*Order)) error {

	b.orderCallBack = callback
	b.subscribeUserData()
	return nil
}

func (b *Binance) SubscribePositions(market Market, callback func(positions []*Position)) error {
	b.positionCallBack = callback
	b.subscribeUserData()
	return nil
}

// EventType 用户获取用户私人数据推送过来的数据类型.
type EventType struct {
	Event string `json:"e"`
	T     int64  `json:"T"`
	E     int64  `json:"E"`
}

// BinancePosition 仓位.
type BinancePosition struct {
	Symbol    string `json:"s"`
	Size      string `json:"pa"`
	CostPrice string `json:"ep"`
}

// EventPosition 仓位信息事件.
type EventPosition struct {
	Event      string `json:"e"`
	UpdateTime int64  `json:"E"`
	A          struct {
		Positions []BinancePosition `json:"P"`
	} `json:"a"`
}

// BinanceOrder .
type BinanceOrder struct {
	ID           int64  `json:"i"`  // ID
	ClientOId    string `json:"c"`  // 客户端订单ID
	Symbol       string `json:"s"`  // 标
	Price        string `json:"p"`  // 价格
	StopPx       string `json:"sp"` // 触发价
	Amount       string `json:"q"`  // 委托数量
	AvgPrice     string `json:"ap"` // 平均成交价
	FilledAmount string `json:"z"`  // 成交数量
	Direction    string `json:"S"`  // 委托方向
	Type         string `json:"ot"` // 委托类型
	Status       string `json:"X"`  // 委托状态
}

// EventOrder .
type EventOrder struct {
	Event      string       `json:"e"`
	UpdateTime int64        `json:"E"`
	Order      BinanceOrder `json:"o"`
}

// subscribeUserData 订阅用户私人数据.
func (b *Binance) subscribeUserData() error {
	if !b.userStream {
		listenKey, err := b.client.NewStartUserStreamService().Do(context.Background())
		if err != nil {
			glog.Error(err)
			fmt.Println(err)
		}
		b.listenKey = listenKey
		wsUserDataHandler := func(message []byte) {
			ep := EventType{}
			// fmt.Println(string(message))
			err := json.Unmarshal(message, &ep)
			if err != nil {
				glog.Error(err)
			}
			if ep.E == b.eventTime {
				return
			}
			b.eventTime = ep.E

			if ep.Event == "ACCOUNT_UPDATE" && b.positionCallBack != nil {
				eb := EventPosition{}
				err := json.Unmarshal(message, &eb)
				if err != nil {
					glog.Error(err)
					fmt.Println(err)
				}
				fmt.Println(eb)
				var positions []*Position
				for _, v := range eb.A.Positions {
					p := &Position{
						Symbol:    v.Symbol,
						Size:      utils.ParseFloat64(v.Size),
						OpenPrice: utils.ParseFloat64(v.CostPrice),
						AvgPrice:  utils.ParseFloat64(v.CostPrice),
					}
					positions = append(positions, p)
				}
				b.positionCallBack(positions)
			}
			if ep.Event == "ORDER_TRADE_UPDATE" && b.orderCallBack != nil {
				oe := EventOrder{}
				err := json.Unmarshal(message, &oe)
				if err != nil {
					glog.Error(err)
				}
				// fmt.Printf("price:%s,amount:%s,side:%s,status:%s\n", oe.Order.Price, oe.Order.Amount, oe.Order.Direction, oe.Order.Status)
				var orders []*Order
				// fmt.Println(oe.Order)
				o := &Order{
					ID:           utils.ParseIntString(oe.Order.ID),
					ClientOId:    oe.Order.ClientOId,
					Symbol:       oe.Order.Symbol,
					Time:         time.Unix(oe.UpdateTime/1000, 0),
					Price:        utils.ParseFloat64(oe.Order.Price),
					StopPx:       utils.ParseFloat64(oe.Order.StopPx),
					AvgPrice:     utils.ParseFloat64(oe.Order.AvgPrice),
					FilledAmount: utils.ParseFloat64(oe.Order.FilledAmount),
					Status:       b.orderStatus(binance.OrderStatusType(oe.Order.Status)),
					Amount:       utils.ParseFloat64(oe.Order.Amount),
					UpdateTime:   time.Unix(oe.UpdateTime/1000, 0),
					Type:         b.convertOrderType(binance.OrderType(oe.Order.Type)),
				}
				if oe.Order.Direction == "SELL" {
					o.Direction = Sell
				} else {
					o.Direction = Buy
				}
				orders = append(orders, o)
				b.orderCallBack(orders)
			}
		}
		errHandler := func(err error) {
			fmt.Println(err)
			glog.Error(err)
		}
		_, _, err = WsUserDataServe(listenKey, wsUserDataHandler, errHandler)
		if err != nil {
			glog.Error(err)
			fmt.Println(err)
		}
		// 定时给listenKey续期
		go func() {
			time.Sleep(1800 * time.Second)
			err := b.client.NewKeepaliveUserStreamService().ListenKey(b.listenKey).Do(context.Background())
			if err != nil {
				glog.Error(err)
			}
		}()

	}
	return nil
}

func NewBinance(params *Parameters) *Binance {
	client := binance.NewClient(params.AccessKey, params.SecretKey)
	b := &Binance{
		client: client,
	}
	if params.ProxyURL != "" {
		b.SetProxy(params.ProxyURL)
	}
	return b
}
