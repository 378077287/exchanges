package gen

const (
	BinanceFutures = "binancefutures"
	Binance        = "binance"
	BitMEX         = "bitmex"
	Deribit        = "deribit"
	Bybit          = "bybit"
	Hbdm           = "hbdm"
	HbdmSwap       = "hbdmswap"
	OkexFutures    = "okexfutures"
	OkexSwap       = "okexswap"
)
