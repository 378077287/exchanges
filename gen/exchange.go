package gen

import (
	"fmt"

	. "gitee.com/378077287/exchanges"
	"gitee.com/378077287/exchanges/binance"
	"gitee.com/378077287/exchanges/binancefutures"
	"gitee.com/378077287/exchanges/bitmex"
	"gitee.com/378077287/exchanges/bybit"
	"gitee.com/378077287/exchanges/hbdm"
	"gitee.com/378077287/exchanges/hbdmswap"
	"gitee.com/378077287/exchanges/okexfutures"
	"gitee.com/378077287/exchanges/okexswap"
)

func NewExchange(name string, opts ...ApiOption) Exchange {
	params := &Parameters{}

	for _, opt := range opts {
		opt(params)
	}

	return NewExchangeFromParameters(name, params)
}

func NewExchangeFromParameters(name string, params *Parameters) Exchange {
	switch name {

	case Bybit:
		return bybit.NewBybit(params)
	case BinanceFutures:
		return binancefutures.NewBinanceFutures(params)
	case Binance:
		return binance.NewBinance(params)
	case BitMEX:
		return bitmex.NewBitMEX(params)
	case Hbdm:
		return hbdm.NewHbdm(params)
	case HbdmSwap:
		return hbdmswap.NewHbdmSwap(params)
	case OkexFutures:
		return okexfutures.NewOkexFutures(params)
	case OkexSwap:
		return okexswap.NewOkexSwap(params)

	default:
		panic(fmt.Sprintf("new exchange error [%v]", name))
	}
}
