module gitee.com/378077287/exchanges

go 1.15

require (
	github.com/MauriceGit/skiplist v0.0.0-20191117202105-643e379adb62
	github.com/adshao/go-binance v0.0.0-20200927114546-097cf4adefa9
	github.com/bitly/go-simplejson v0.5.0
	github.com/chuckpreslar/emission v0.0.0-20170206194824-a7ddd980baf9
	github.com/frankrap/bitmex-api v1.0.1
	github.com/frankrap/bybit-api v1.0.2
	github.com/frankrap/huobi-api v1.0.2
	github.com/frankrap/okex-api v1.0.4
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/gorilla/websocket v1.4.2
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/micro/go-micro v1.18.0 // indirect
	github.com/pelletier/go-toml v1.4.0 // indirect
	github.com/rocketlaunchr/dataframe-go v0.0.0-20200914013621-fd3cc83fba45
	github.com/sony/sonyflake v1.0.0
	github.com/spf13/cast v1.3.1
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/tidwall/gjson v1.6.1 // indirect
	golang.org/x/text v0.3.3 // indirect
)
